
# stuff that needs jquery
$ ->

  
# Vanilla javascript
document.addEventListener 'turbolinks:load', ->
  $('[data-toggle="tooltip"]').tooltip()
  $('.notice').delay(5000).fadeOut 1500
  $('.alert_m').delay(5000).fadeOut 1500

  # syntax highlight for html code tag 
  $('pre code').each (i, block) ->
    hljs.highlightBlock block
   


  cancel_btn = document.querySelector('.cancel_button')
  div_alert = document.querySelector('.alert')
  div_notice = document.querySelector('.notice')
  if cancel_btn != null
    cancel_btn.addEventListener 'click', ->
      if div_notice != null
        div_notice.style.display = 'none'
      if div_alert != null
        div_alert.style.display = 'none'
