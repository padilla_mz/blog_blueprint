class HomeController < ApplicationController
  def index
    # @q = Article.default.ransack(params[:q])
    # @articles = @q.result.paginate(page: params[:page], per_page: 11).order('created_at desc')

    @pagy, @articles = pagy(@q.result.order('publication_day desc'), items: 11)

    @ar = Article.default

  end

  def show_article
    begin

  		@article = Article.where(published: true).find(params[:id])
      @article.increment!(:views, 1)
      puts '-----------------'
      puts request.remote_ip  		
  	rescue ActiveRecord::RecordNotFound => e
  		puts '-----'
  		puts e
  		puts '-----'
  		return (redirect_to root_path, notice: "Not found")
  	end
  	
  end

  def feed

    @articles = Article.default

    respond_to do |f|
      f.rss {render layout: false} 
    end

  end

end
