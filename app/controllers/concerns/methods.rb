module Methods

	def only_super
		if current_admin.role != 'super'
			return (redirect_to articles_path, notice: "You don't have access")
		end
	end

  def only_super_or_admin
    if !(current_admin.role == 'super' || current_admin.role == 'admin')
      return (redirect_to articles_path, notice: "You don't have access")
    end
  end

end