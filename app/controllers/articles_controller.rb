class ArticlesController < ApplicationController
  before_action :authenticate_admin!

  layout 'dash'

  # Get requests
  def index
    @q = Article.ransack(params[:q])
    @pagy, @articles = pagy(@q.result.order('created_at desc'))


  end
  def show
    @article = Article.find(params[:id])
  end

  def new
    @article = Article.new
  end

  def edit
    @article = Article.find(params[:id])
    unless (current_admin.id == @article.admin_id) or (current_admin.role == 'admin') or (current_admin.role == 'admin')
      return (redirect_to action: :index, notice: "You can't do that")
    end

  end

  # post requests ...
  def create
    # @article = Article.new(articles_params)
    @article = current_admin.articles.new(articles_params)
    @article.text_compiled = Kramdown::Document.new(@article.text).to_html

    @article.tags.delete_all
    tag_list = params['article']['tags']
    tag_list.shift

    tag_list.each do |tag_name|
      tag = Tag.find_by!(name: tag_name)
      @article.tags << tag
    end


    if @article.save
      redirect_to @article
    else
      render 'new'
    end
    # render plain: params[:article].inspect
  end


  def update
    @article = Article.find(params[:id])
    @article.assign_attributes(articles_params)
    @article.text_compiled = Kramdown::Document.new(@article.text).to_html


    # @article.tags.delete_all
    @article.tags.delete_all
    tag_list = params['article']['tags']
    tag_list.shift

    p '---------------'
    p tag_list
    p '---------------'

    tag_list.each do |tag_name|
      tag = Tag.find_by!(name: tag_name)
      @article.tags << tag
    end


    @article.text_compiled = Kramdown::Document.new(@article.text).to_html

    if @article.save
      redirect_to @article
    else
      render 'edit'
    end
  end


  def destroy
    @article = Article.find(params[:id])
    @article.cover.purge
    @article.destroy
 
    redirect_to articles_path
  end

  private
    def articles_params
      params.require(:article).permit(:publication_day, :admin_id, :title, :text, :cover, :url_cover, :url_img1, :url_img2, :url_img3, :published, :category, :short_desc)
    end
end
