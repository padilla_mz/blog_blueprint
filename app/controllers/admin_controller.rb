include Methods

class AdminController < ApplicationController
	before_action :authenticate_admin!, :only_super_or_admin

  layout 'dash'
  
  def panel
    @posts = Article.default.count
    # @comments = Comment.all
  end

  def index_users
  	@q = Admin.ransack(params[:q])
  	@pagy, @users = pagy(@q.result.order('created_at desc'))
  end

  def new_user
  	@user = Admin.new
  end

  def edit_user
  	@user = Admin.find(params[:id])
  	
  end


  # Post, patch, delete ....
  	
  def create_user
  	@user = Admin.new(admin_params)

  	if @user.save
  		redirect_to action: 'index_users', notice: "User created!"
  	else
  		render 'new_user'
  	end
  end

  def update_user
  	@user = Admin.find(params[:id])
  	if @user.update(admin_params)
  		redirect_to action: 'index_users', notice: "User updated!"
  	else
  		render 'edit_user'
  	end
  	
  end

  def destroy_user
  	@user = Admin.find(params[:id])
  	@user.destroy

  	return (redirect_to action: 'index_users', notice: "User deleted!")
  end

  private

  	def admin_params
  		params.require(:admin).permit(:role, :full_name, :username, :p_twitter, :p_instagram, :email, :password)
  	end

end
