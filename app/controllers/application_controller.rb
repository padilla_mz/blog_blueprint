class ApplicationController < ActionController::Base
	include Pagy::Backend
	before_action :set_stuff

	def set_stuff
		@q = Article.default.ransack(params[:q])
		@setting = Setting.first
	end
end
