include Methods

class SettingController < ApplicationController
	before_action :authenticate_admin!, :only_super

  layout 'dash'
  
	def edit
		

	end

	def update
		if @setting.update(settings_params)
			return (redirect_to(edit_settings_path, notice: "Settings updated!"))
		else
			render 'edit'
		end
	end

	private

    def settings_params
      params.require(:setting).permit(:disqus_id, :name, :g_analytics, :post_page)
    end
end
