class Admin < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :lockable, :timeoutable,
         :recoverable, :trackable, :validatable

  has_many :articles

 	validates :full_name, :username, :p_twitter, :p_instagram, uniqueness: true
 	validates :full_name, :username, presence: true
 	validates :email, presence: true

  # scope :activos_al_momento, -> {self.where(last_sign_in_at: (Date.today.beginning_of_day)..(DateTime.now)).count}
  # scope :activos_por_mes_promedio, -> {self.where(last_sign_in_at: (Date.today.beginning_of_month)..(Date.today.end_of_month)).count}

  Roles = [:admin, :editor]
end
