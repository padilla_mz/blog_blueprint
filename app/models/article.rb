class Article < ApplicationRecord
	
	has_one_attached :cover
  belongs_to :admin
	has_many :comments, dependent: :destroy
	has_and_belongs_to_many :tags

	validates :title, presence: true, length: {in: 10..40}
	validates :text, :category, :publication_day, :cover, presence: true

	Categories = [['Code', 'code'], ['Random', 'random'], ['Tech', 'tech'], ['Science', 'science']]

	scope :default, -> {where(published: true).where('publication_day < ?', DateTime.now).order('created_at desc')}
end
