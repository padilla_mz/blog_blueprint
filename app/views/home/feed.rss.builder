#encoding: UTF-8

xml.instruct! :xml, :version => "1.0"
xml.rss :version => "2.0" do
  xml.channel do
    xml.title "bitSoup"
    xml.author "Angel Padilla"
    xml.description "Software-Development, Mobile, Technology"
    xml.link "https://www.bitsoup.ml"
    xml.language "en"

    for article in @articles
      xml.item do
        xml.title article.title
        xml.author article.admin.full_name
        xml.pubDate article.publication_day.to_s(:rfc822)
        xml.link "https://www.bitsoup.com/f/" + article.id.to_s
        xml.guid article.id

        text = article.text
        # if you like, do something with your content text here e.g. insert image tags.
        # Optional. I'm doing this on my website.
        if article.cover.attached?
            image_url = url_for(article.cover)
            image_caption = ''
            image_align = ""
            image_tag = "
                <p><img src='" + image_url +  "' alt='" + image_caption + "' title='" + image_caption + "' align='" + image_align  + "' /></p>
              "
            text = text.sub('{image}', image_tag)
        end
        xml.description "<p>" + text + "</p>"

      end
    end
  end
end