Rails.application.routes.draw do
  devise_for :admins
  root 'home#index'
  # get 'admin'
  # get 'home/index'

  # get 'admin', to: 'admin#panel', as: :admin_home

  scope 'f' do
    get ':id', to: 'home#show_article', as: :show_article
  end

  get 'feed', to: "home#feed"



  scope 'admin' do
    resources :articles do
      resources :comments
    end
    get '', to: 'admin#panel', as: :admin_home
    get 'settings', to: 'setting#edit', as: :edit_settings
  	patch 'settings/update', to: 'setting#update', as: :update_settings

    get 'editors', to: "admin#index_users", as: :editors
    get 'editor/new-editor', to: "admin#new_user", as: :editor_new
    get 'editor/:id/edit', to: "admin#edit_user", as: :editor_edit
    post 'editor/create', to: "admin#create_user", as: :editor_create
    patch 'editor/update/:id', to: "admin#update_user", as: :editor_update
    delete 'editor/delete', to: "admin#destroy_user", as: :destroy_editor
  end

end
