class AddTextCompiledToArticles < ActiveRecord::Migration[5.2]
  def change
  	add_column :articles, :text_compiled, :text
  end
end
