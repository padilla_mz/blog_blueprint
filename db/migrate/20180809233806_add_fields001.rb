class AddFields001 < ActiveRecord::Migration[5.2]
  def change
  	add_reference :articles, :admin, index: true
  	add_reference :comments, :admin, index: true
  end
end
