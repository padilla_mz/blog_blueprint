class CreateSettings < ActiveRecord::Migration[5.2]
  def change
    create_table :settings do |t|
      t.string :disqus_id, default: ''
      t.string :g_analytics, default: ''
      t.string :name, default: "Blog"
      t.integer :post_page, default: 20

      t.timestamps
    end
  end
end
