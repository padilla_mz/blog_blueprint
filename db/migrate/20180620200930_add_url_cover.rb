class AddUrlCover < ActiveRecord::Migration[5.2]
  def change
  	add_column :articles, :url_cover, :string

  	add_column :articles, :url_img1, :string
  	add_column :articles, :url_img2, :string
  	add_column :articles, :url_img3, :string
  end
end
